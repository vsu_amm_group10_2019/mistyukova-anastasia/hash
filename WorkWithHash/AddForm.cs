﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class AddForm : Form
    {
        public People People { get; } = new People();
        private FormState FormState;
        public AddForm(FormState formState, People people = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        return;
                    }
                case FormState.EDIT:
                    {
                        tbFIO.Text = people.FIO;
                        tbPhone.Text = people.Phone.ToString();
                        tbAddress.Text = people.Address;
                        Text = "Edit people";
                        break;
                    }
                case FormState.DELETE:
                   
                case FormState.SEARCH:
                    {
                        tbAddress.ReadOnly = true;
                        tbFIO.ReadOnly = true;
                        tbPhone.ReadOnly = false;
                        Text = "Search people";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        tbFIO.Text = people.FIO;
                        tbPhone.Text = people.Phone.ToString();
                        tbAddress.Text = people.Address;

                        tbAddress.ReadOnly = true;
                        tbFIO.ReadOnly = true;
                        tbPhone.ReadOnly = true;
                        Text = "Show people";
                        break;
                    }
                    
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(FormState == FormState.ADD || FormState == FormState.EDIT)
            {
                People.FIO = tbFIO.Text;
                People.Address = tbAddress.Text;
            }
            long phone = 0;
            if (!long.TryParse(tbPhone.Text, out phone) && phone < 0)
            {
                MessageBox.Show("Phone must be valid!");
                return;
            }
            People.Phone = phone;
            DialogResult = DialogResult.OK;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
