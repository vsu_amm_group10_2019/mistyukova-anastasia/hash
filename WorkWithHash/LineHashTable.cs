﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    class LineHashTable : IHashTable
    {
        private Elem[] Table { get; }
        public int Size { get; }
        public LineHashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Elem[size];
        }
        public bool Add(People people)
        {
            if (Find(people.Phone) != null)
            {
                return false;
            }
            int index = (int)(people.Phone % Size);
            if (Table[index] == null || Table[index].Deleted) // Добавление в таблицу
            {
                Table[index] = new Elem()
                {
                    People = people
                };
                return true;
            }
            for (int i = index + 1; i < Size; i++) // Метод решения коллизии
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        People = people
                    };
                    return true;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Elem()
                    {
                        People = people
                    };
                    return true;
                }
            }
            return false;
        }
        public People Find(long phone)
        {
            int index = (int)(phone % Size);
            if (Table[index] == null)
            {
                return null; 
            }
            if (!Table[index].Deleted && Table[index].People.Phone == phone)
            {
                return Table[index].People;
            }
            for (int i = index + 1; i < Size; i++) 
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].People.Phone == phone)
                {
                    return Table[i].People;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].People.Phone == phone)
                {
                    return Table[i].People;
                }
            }
            return null;
        }
        public void Delete (long phone)
        {
            int index = (int)(phone % Size);
            if (Table[index] == null)
            {
                return;
            }
            if (!Table[index].Deleted && Table[index].People.Phone == phone)
            {
                Table[index].Deleted = true;
                return;
            }
            for (int i = index + 1; i < Size; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].People.Phone == phone)
                {
                    Table[i].Deleted=true;
                    return;
                }
            }
            for (int i = 0; i < index; i++)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].People.Phone == phone)
                {
                    Table[i].Deleted = true;
                    return;
                }
            }
        }


        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<People> GetData()
        {
            List < People > list = new List<People>();
            foreach(Elem e in Table)
            {
                if(e != null && !e.Deleted)
                {
                    list.Add(e.People);
                }
            }
            return list;
        }
        public string PeopleTableToText()
        {
            string text = "";
            foreach (Elem elem in Table)
            {
                if (elem != null && !elem.Deleted)
                {
                    text += elem.People.FIO + "\n";
                    text += elem.People.Address + "\n";
                    text += elem.People.Phone + "\n";
                }
            }
            return text;
        }
        public void GetPeopleFromText(string text)
        {
            string[] strArr = text.Split(Environment.NewLine.ToCharArray());
            int i = 0;
            while (i < strArr.Length - 2)
            {
                People people = new People();
                people.FIO = strArr[i++];
                people.Address = strArr[i++];
                people.Phone = long.Parse(strArr[i++]);
                this.Add(people);
            }
        }
    }


    class Elem
    {
        public People People { get; set; }
        public bool Deleted { get; set; } = false;

    }
}
