﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkWithHash
{
    public partial class MainForm : Form
    {
        LineHashTable LineHash { get; set; } = new LineHashTable(100);
        string Filename = "";
        
        public MainForm()
        {
            InitializeComponent();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.ADD);
            if (addForm.ShowDialog() == DialogResult.OK)
            {
                LineHash.Add(addForm.People);
            }
            Redraw();
        }

        private void Redraw()
        {
            List<People> peoples = LineHash.GetData();
            Tablepeople.Rows.Clear();
            Tablepeople.RowCount = peoples.Count;
            for(int i = 0; i < peoples.Count; i++)
            {
                Tablepeople.Rows[i].Cells[0].Value = peoples[i].FIO;
                Tablepeople.Rows[i].Cells[1].Value = peoples[i].Phone.ToString();
                Tablepeople.Rows[i].Cells[2].Value = peoples[i].Address;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            long id;
            if (search.ShowDialog() == DialogResult.OK)
            {
                id = search.People.Phone;
            }
            else
            {
                return;
            }
            People people = LineHash.Find(id);
            if(people != null)
            {
                AddForm edit = new AddForm(FormState.EDIT, people);
                
                if (edit.ShowDialog() == DialogResult.OK)
                {
                    LineHash.Delete(id);
                    LineHash.Add(edit.People);
                    Redraw();
                }
            } else
            {
                MessageBox.Show("people not found");
            }
            

        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            long phone;
            if (search.ShowDialog() == DialogResult.OK)
            {
                phone = search.People.Phone;
            }
            else
            {
                return;
            }
            
            People people = LineHash.Find(phone);
            if (people != null)
            {
                AddForm show = new AddForm(FormState.DISPLAY, people);
                show.ShowDialog();
            }
            else
            {
                MessageBox.Show("people not found");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.SEARCH);
            long phone;
            if (search.ShowDialog() == DialogResult.OK)
            {
                phone = search.People.Phone;
            }
            else
            {
                return;
            }
            LineHash.Delete(phone);
            Redraw();
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LineHash = new LineHashTable(100);
            Filename = "";
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Filename = openFileDialog.FileName;
                    LineHash.GetPeopleFromText(File.ReadAllText(Filename));
                    Redraw();
                }
            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Filename == "")
            {
                using (SaveFileDialog sfg = new SaveFileDialog())
                {
                    if (sfg.ShowDialog() == DialogResult.OK)
                    {
                        Filename = sfg.FileName;
                        File.WriteAllText(Filename, LineHash.PeopleTableToText());
                    }
                }
                return;
            }
            File.WriteAllText(Filename, LineHash.PeopleTableToText());
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfg = new SaveFileDialog())
            {
                if (sfg.ShowDialog() == DialogResult.OK)
                {
                    Filename = sfg.FileName;
                    File.WriteAllText(Filename, LineHash.PeopleTableToText());
                }
            }
        }
    }
}
