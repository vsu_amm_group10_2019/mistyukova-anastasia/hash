﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    interface IHashTable
    {
        bool Add(People people);
        People Find(long phone);
        void Delete(long phone);
        List<People> GetData(); 
        void Clear();
    }
}
