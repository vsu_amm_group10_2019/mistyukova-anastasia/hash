﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkWithHash
{
    public class People
    {
        public string FIO { get; set; }
        public long Phone { get; set; }
        public string Address { get; set; }

    }
}
